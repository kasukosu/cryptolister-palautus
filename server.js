'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const authRoutes = require('./routes/auth-routes');
const favoriteRoutes = require('./routes/favorite-routes');
const passportSetup = require('./config/passport-setup');
const mongoose = require('mongoose');
const keys = require('./config/keys');
const cookieSession = require('cookie-session');
const passport = require('passport')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('static'));
app.set('view engine', 'ejs');

const authCheck = (req, res , next) => {
    if (!req.user){
        res.redirect("/auth/login");
    } else {
        next();
    }
};

app.use(cookieSession({
	maxAge: 24 * 60 * 60 * 1000,
	keys: [keys.session.cookieKey]
}));

//initialize passport
app.use(passport.initialize());
app.use(passport.session());

//connect to mongodb
mongoose.connect(keys.mongodb.dbURI, () => {
	console.log('connected to mongodb');
})

//Use authRoutes and favoriteRoutes when they are needed
app.use('/auth', authRoutes);
app.use('/favorite', favoriteRoutes);

app.get('/', (req, res) => {
	res.render('home');
});

app.get('/profile', authCheck, (req, res) => {
	res.render('profile');
})

//Initialize the server to port 3000
app.listen(3000, () => {
	console.log('app now listening for request on port 3000')
})

