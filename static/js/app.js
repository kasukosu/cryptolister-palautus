

				/*
		Start of Vue.js application
		Manages the front-end of the whole website
		*/
		//URL for the api that gets the coin icons
		let CRYPTOCOMPARE_API_URL = "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=100&tsym=USD&API_KEY{dc24eb951aee25ad15748801681171dd3fc8723c80aca5bb3a66f572bec81d5f}"
		let CRYPTOCOMPARE_LOGOS_API_URL = "https://www.cryptocompare.com/"
		const proxyurl = "https://cors-anywhere.herokuapp.com/"

		//URL for the API that gets the price data
		let COINMARKETCAP_API_URL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=3c82f7a4-f3d9-4144-9696-57cb3b977dd3"

		//Updates data from api every 2 minutes
		let UPDATE_INTERVAL = 60 * 2000;
	
		//Vue filter that lets us convert our values to currency form
		Vue.filter('toCurrency', function (value) {
			if (typeof value !== "number") {
				return value;
			}
			
			var formatterBig = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: 'USD',
				minimumFractionDigits: 0

			});
			var formatterSmall = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: 'USD',
				minimumFractionDigits: 4
			});
			
			if (value<1){
				return formatterSmall.format(value);
			}else{
				return formatterBig.format(value);
			}
		});
		
		//Vue filter that rounds decimals
		Vue.filter('round', function(value, decimals) {
			if(!value) {
				value = 0;
			}

			if(!decimals) {
				decimals = 0;
			}

			value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
			return value;
		});
		
		//Creates the Vue instance	 
		let app = new Vue({
			el: "#app",
			data: {
				coinName: '',
				coinsData: [],
				cmcCoinsData: [],
				favorites: [],
				favoritesList: [],
				favoriteNames: [],
				tempUrl:''
			},
			

			methods: {

				/*
				* This method loads all coindata from the CryptoCompare API, After getting
				* the symbol of a coin we use that to look up the logo from the other API
				*/
				async getCoinsData() {
					let self = this;
					NProgress.start();
					let JSON = await axios.get(proxyurl + CRYPTOCOMPARE_API_URL);
					this.coinsData = JSON.data.Data;
					console.log("Coin data retrieved")
					NProgress.done();

				},
				
				
				//get array of favorites from database
				async getFavorites() {
					let favorites = await axios.get('/favorite/load');
					this.favoriteNames = [];
						for (var i = 0; i < favorites.data.length; i++) {
							this.favoriteNames.push(favorites.data[i].coinName);	

						}
						console.log("Favorites retrieved");
						//Start creating the list when both async tasks have completed
						app.createFavoriteList();

				},
			
				//populates a new array of your favorite coin objects 
				createFavoriteList: function(){
					console.log("Favorite list created");
					//empty the favorite array before updating it
					this.favoritesList=[];
					
						for(var i = 0; i < this.favoriteNames.length; i++){
							console.log(this.favoriteNames[i]);
							for(var j = 0; j < this.coinsData.length; j++){
								if(this.coinsData[j].CoinInfo.FullName == this.favoriteNames[i]){
									tempObj = this.coinsData[j];
									this.favoritesList.push(tempObj);
									break;
								}
							}
					
						}
				},

				

				//add new favorite coin to database, also runs the function that refreshes the favorite list
				createFavorite(fullname){

					axios.post('/favorite/add', {coin: fullname})
					.then((response)=>{
						console.log(response);
						if(response.data == "login first"){
							window.location.href = "/auth/login";
						}
						if(response.data == "alreadyFavorite"){
							M.toast({html: fullname+' is already in your favorites'});
						}else{
							M.toast({html: fullname+' added to favorites'});
							app.getFavorites();
						}
						
						
					}).catch((error) => {
						console.log(error);
					})
					

				},
				
				//Function to clear favorite list NOT USED AT THIS MOMENT
				clearFavorites(){
					app.favorites=[];
				},

				//Deletes the given favorite coin from the database
				async deleteFavorite(coinName) {
					console.log("deleting" + coinName);
					await axios.post('/favorite/delete', {coin: coinName})
					app.getFavorites();
					M.toast({html: coinName+' has been deleted from your favorites'});

				},
				
			

				/*
				* Looks up the logo of the given coin by its name
				*/
				getCoinImage(name) {
						for(var i = 0; i < this.coinsData.length; i++){
							if(this.coinsData[i].CoinInfo.FullName == name){
								tempUrl = this.coinsData[i].CoinInfo.ImageUrl;
								break;
							}
						}
						return CRYPTOCOMPARE_LOGOS_API_URL + tempUrl;

				},

				//Changes the percent number color by it's value: red if negative, green if positive
				getColor: (num) => {
					return num > 0 ? "color:green;" : "color:red;";
				}
			},
			created () {
				this.getCoinsData();

			},
			mounted () {
				this.getFavorites();
			},
			
			
		});
		//update the coindata by the set time UPDATE_INTERVAL
		setInterval(() => {
			app.getCoinsData();

		}, UPDATE_INTERVAL);

		
		
