Crypto Lister is a website made by using HTML, Vue.js and axios.

To install the web page on docker:

Download project from Github and build it in Docker. COMMAND:
docker build -t cryptolister https://kasukosu@bitbucket.org/kasukosu/cryptolister-palautus.git

This will create docker image named "cryptolister".

Final step is to run above image. COMMAND:
docker run -p 3000:3000 -d cryptolister

Now your app is seen here: http://localhost:3000

![mainpage](static/img/mainpage.png)
![login](static/img/login.png)
![favorites](static/img/favorites.png)