'use strict'
const router = require('express').Router();
const User = require('../models/user-model');
const Favorite = require('../models/favorite-model');

//Check if user has already logged in
const authCheck = (req, res , next) => {
    if (!req.user){
        res.send('login first')
        res.redirect("/auth/login");
    } else {
        next();
    }
};

// check what user is logged in
router.get('/', authCheck, (req, res) => {
    res.send('User is : '+req.user);
});

router.post('/add', authCheck, (req, res) => {
    //Check if the user has already liked the coin before
    Favorite.findOne({
        googleId: req.user.googleId,
        coinName: req.body.coin
    }).then((alreadyFavorite) => {
        if(alreadyFavorite){
            //already have the favorite added
            console.log('Already favorite', alreadyFavorite);
            res.send("alreadyFavorite");
        } else {
            // if not, create a new favorite to db
            new Favorite({
                googleId: req.user.googleId,
                coinName: req.body.coin,
            }).save().then((newFavorite) => {
                res.send(newFavorite);
                console.log(req.user.googleId);
                console.log('Added new favorite:' + newFavorite);
            })
        }
    })
});

//Load favorites from database
router.get('/load', authCheck, (req, res) => {
    Favorite.find({
        googleId: req.user.googleId
    }).then((result) => {
        console.log(result);
        res.send(result);
    })
});

//Delete favorites from database
router.post('/delete', authCheck, (req, res) => {
    Favorite.deleteOne({
        googleId: req.user.googleId,
        coinName: req.body.coin,
    }).then((result) => {
        res.send(result);
    })
});

module.exports = router;