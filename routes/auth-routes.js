'use strict'
const router = require('express').Router();
const passport = require('passport');
//auth login
router.get('/login', (req, res) => {
    res.render('login');
});

//auth logout
router.get('/logout', (req, res) => {
    //handle with passport
    req.logout();
    res.redirect('/');
});

//auth with google
router.get('/google', passport.authenticate('google', {
    scope: ['profile']
}));

//Redirect from google auth
router.get('/google/redirect', (req, res) => passport.authenticate('google', { successRedirect: '/',
failureRedirect: '/login',})(req,res));

module.exports = router;