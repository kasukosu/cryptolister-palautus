const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const favoriteSchema = new Schema({
    googleId: String,
    coinName: String,
});

const Favorite = mongoose.model('favorite', favoriteSchema);

module.exports = Favorite;